@ECHO off

REM ---------- The directory of test.bat file -------------
set cwd=%cd%
REM ------------ The directory of source files ------------
set SRC_PATH=%cwd%\source
REM ------------ The directory of executable file ------------
set EXE_PATH=%cwd%\source\build

REM ------------ Go to the Specified function -------------
if /I %1 == build goto :build
if /I %1 == run goto :run

REM ---------------------- BUILD --------------------------
:build
cd %SRC_PATH%
make all
goto :eof

REM ---------------------- RUN --------------------------
:run
cd %EXE_PATH%
hello_world
goto :eof

:eof
REM --------------------- End of Batch File ------------------